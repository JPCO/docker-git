#!/bin/sh
if [ -z "$1" ]; then 
	echo "provide site to reload"
	exit 1
fi
if [ -x /var/workspace/$1 ]; then
	cd /var/workspace/$1
	date >> /var/workspace/reload_$1.txt
	git pull >> /var/workspace/reload_$1.txt 2>&1
fi
