#!/bin/sh
if [ ! $# = 3 ]; then 
	echo "usage: $0 git-url branch-name name"
	exit 1
fi
echo "git clone -b $2 $1 $3"

git clone -b $2 $1 $3
